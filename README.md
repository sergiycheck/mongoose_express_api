
### Installation
1. Clone repo
2. Open project directory
3. run ```npm install``` command
3. run ```npm start``` command

### Features

1. CRUD for users.

2. Get user by id with populated articles field.

3. Get all articles that created by specific user.

4. CRUD for articles.

5. Creating new article, with check if owner exist. 
Create an article and increment ***numberOfArticles*** field for that user.

6. Edit any article document. Check if article / user exist, and only
after that start updating document.

7. Search for articles using next filters **title, subtitle, description, owner, category, createdAt, updatedAt**. If the request endpoint is send without setting filter criteria, get all articles from database with populated owner field.

8. Delete any article from database with decreasing ***numberOfArticles*** field for user that created this article.

9. Api endpoints is tested with supertest library.



